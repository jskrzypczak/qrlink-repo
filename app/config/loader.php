<?php

$loader = new \Phalcon\Loader();

$loader->registerNamespaces(
    [
        'QrCode\Models' => APP_PATH . "/models",
        'QrCode\Forms' => APP_PATH . "/forms",
        'QrCode\Validators' => APP_PATH . "/validators",
        'QrCode\Services' => APP_PATH . "/services",
    ]
);

$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir
    ]
)->register();

include APP_PATH . "/../vendor/autoload.php";