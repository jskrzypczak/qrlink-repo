<?php

namespace QrCode\Forms\User;

use QrCode\Forms\FormBase;
use Phalcon\Forms\Element as Element;
use Phalcon\Validation\Validator as Validator;

class ChangePasswordForm extends FormBase
{
    public function initialize()
    {
        $current = new Element\Password('current');
        $new = new Element\Password('new');
        $reNew = new Element\Password('re-new');


        $current->addValidator(new Validator\PresenceOf([
            'message' => 'Treść nie może być pusta'
        ]));

        $new->addValidator(new Validator\PresenceOf([
            'message' => 'Treść nie może być pusta'
        ]));

        $reNew->addValidator(new Validator\PresenceOf([
            'message' => 'Treść nie może być pusta'
        ]));

        $reNew->addValidator(new Validator\Confirmation([
            'message' => 'Hasła nie sa takie same',
            'with'  => 'new'

        ]));

        $this->add($current);
        $this->add($new);
        $this->add($reNew);
    }
}



