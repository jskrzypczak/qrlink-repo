<?php

use QrCode\Forms\User\ChangePasswordForm;
use QrCode\Models\User\User;

class UserController extends ControllerBase
{

    public function profileAction()
    {
    }

    public function passwordAction()
    {
        $form = new ChangePasswordForm();

        if ($form->isSubmittedAndValid()) {
            $user = User::getCurrentUser();

            if($user->isPasswordCorrect($this->request->getPost('current'))) {
                $user->changePassword($this->request->getPost('new'));
                $this->flashSession->success('Hasło zostało zmienione!');
                $form->clear();
            } else {
                $form->addMessageToField('current', new \Phalcon\Validation\Message('Hasło nie jest poprawne'));
            }
        }

        $this->view->setVar('form', $form);
    }
}
