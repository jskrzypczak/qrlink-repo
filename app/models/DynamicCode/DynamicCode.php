<?php

namespace QrCode\Models\DynamicCode;

use Phalcon\Http\Request\File;
use Phalcon\Mvc\Model;
use PHPQRCode\Constants;
use QrCode\Models\NameTry\NameTry;
use QrCode\Models\User\User;
use Phalcon\Di;
use QrCode\Services\QR\QRCreator;

class DynamicCode extends Model
{
    public $id;
    public $name;
    public $target;
    public $argument;
    public $public_url;
    public $user_id;
    public $filename;
    public $created_at;
    private $visitCount;


    public function getSource()
    {
        return 'dynamic_code';
    }


    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function createFromData($post, File $file = null)
    {
        $this->name = $post['text'];
        $this->target = $post['target'];
        $this->argument = $post['url'];
        $this->public_url = 'http://' . $_SERVER['HTTP_HOST'] . '/to/' . $post['url'];
        $this->user_id = User::getCurrentUserId();
        $this->created_at = date('Y-m-d H:i:s');

        /** @var QRCreator $creator */
        $creator = Di::getDefault()->getShared('QRCreator');

        if (empty($file->getName())) {
            $qr = $creator->create($this->public_url, $post['fill'], $post['background'], Constants::QR_ECLEVEL_L);
        }
        else {
            $qr = $creator->create($this->public_url, $post['fill'], $post['background'], Constants::QR_ECLEVEL_H);
            $qr = $creator->addImageToQR($qr, $file);
        }

        $filename = $creator->saveQR($qr);
        $this->filename = $filename;

        if (!empty($post['firstTry'])) {
            $try = new NameTry();
            $try->first = $post['firstTry'];
            $try->last = $post['url'];
            $try->save();
        }

        $this->save();
        return $this->id;
    }

    public function getVisitCount()
    {
        if (empty($this->visitCount)) {

            $count = Di::getDefault()->getShared('db')->fetchOne("SELECT COUNT(*) AS total FROM redirect WHERE dynamic_code_id = $this->id");
            $this->visitCount = $count['total'];
        }

        return $this->visitCount;
    }
}
