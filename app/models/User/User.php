<?php

namespace QrCode\Models\User;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Di;

class User extends Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=false)
     */
    public $email;

    /**
     *
     * @var string
     * @Column(type="string", length=60, nullable=false)
     */
    public $password;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("qrcode");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Model\ResultsetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return User
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function getByEmail($email) {
        return self::findFirst(["email = '$email'"]);
    }

    public function isPasswordCorrect($password)
    {
        if (password_verify($password, $this->password)) {
            return true;
        }
        return false;
    }

    public function login()
    {
        $user = $this->toArray($this);
        $this->getDI()->getShared('session')->set('user', $user);
    }

    public static function isLogged()
    {
        $user = Di::getDefault()->getShared('session')->get('user');
        if ($user) {
            return true;
        }
        return false;
    }

    public static function getCurrentUserId()
    {
        if (self::isLogged()) {
            $user = Di::getDefault()->getShared('session')->get('user');
            return $user['id'];
        }

        return false;
    }

    public static function getCurrentUser()
    {
        if (self::isLogged()) {
            return self::findFirst(self::getCurrentUserId());
        }

        return false;
    }

    public function changePassword($newPassword)
    {
        $this->password = password_hash($newPassword, PASSWORD_BCRYPT);
        $this->update();
    }
}
