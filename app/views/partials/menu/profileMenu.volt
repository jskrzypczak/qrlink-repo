<li><a href="{{ url(['for': 'help']) }}">Pomoc</a></li>
<li><a href="{{ url(['for': 'contact']) }}">Kontakt</a></li>
{% if session.get('user') is null %}
    <li><a href="{{ url(['for': 'login']) }}">Zaloguj</a></li>
{% else %}
    <li class="dropdown">
        <a href="#" class="dropdown-toggle"
           data-toggle="dropdown"> Profil
            <b class="caret"> </b>
            <ul class="dropdown-menu">
                <li class="dropdown-header">{{ session.get('user')['email'] }}</li>
                <li><a href="{{ url(['for': 'panel']) }}">Panel</a></li>
                <li><a href="{{ url(['for': 'user-profile']) }}">Ustawienia</a></li>
                <li><a href="{{ url(['for': 'logout']) }}">Wyloguj</a></li>
            </ul>
        </a>
    </li>
{% endif %}