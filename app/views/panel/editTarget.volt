{% extends "layouts/panel.volt" %}

{% block contentBox %}
    <div>
        <h3>Zmiana celu</h3>
    </div>

    <p>Aktualny cel: <a target="_blank" href="{{ code.target }}">{{ code.target }}</a></p>

    <form method="post">
        <input id="target" type="text" name="target" placeholder="Nowy cel">
        <input id="save" type="submit" value="Zapisz">
    </form>
    {%  if error is not null %}
        <div class="error">{{ error }}</div>
    {% endif %}
{% endblock %}
