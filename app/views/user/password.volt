{% extends "layouts/user.volt" %}

{% block contentBox %}

    <h4>Zmiana hasła</h4>

    {{ flashSession.output() }}

    <form id="static-qr-form" method="post">
        <fieldset class="form-fieldset">
            <div class="form-row">
                <label class="form-label" for="text">Aktualne hasło:</label>
            </div>
            {{ form.render("current", {"class": "form-field", "placeholder": ""}) }}
            {% for message in form.getMessagesFor("current") %}
                <div class="form-error">{{ message }}</div>
            {% endfor %}
        </fieldset>

        <fieldset class="form-fieldset">
            <div class="form-row">
                <label class="form-label" for="text">Nowe hasło:</label>
            </div>
            {{ form.render("new", {"class": "form-field", "placeholder": ""}) }}
            {% for message in form.getMessagesFor("new") %}
                <div class="form-error">{{ message }}</div>
            {% endfor %}
        </fieldset>

        <fieldset class="form-fieldset">
            <div class="form-row">
                <label class="form-label" for="text">Powtórz nowe hasło:</label>
            </div>
            {{ form.render("re-new", {"class": "form-field", "placeholder": ""}) }}
            {% for message in form.getMessagesFor("re-new") %}
                <div class="form-error">{{ message }}</div>
            {% endfor %}
        </fieldset>
        <input type="submit" id="generate" value="Zapisz">
    </form>
{% endblock %}
