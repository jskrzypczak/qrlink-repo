{% extends "layouts/index.volt" %}

{% block contentBox %}
    <h3>Aplikacje:</h3>
    <div>
        <p><a href="https://play.google.com/store/apps/details?id=tw.mobileapp.qrcode.banner">QR Code Reader</a></p>
        <p><a href="https://play.google.com/store/apps/details?id=com.application_4u.qrcode.barcode.scanner.reader.flashlight">Kod QR skanera Błyskawica</a> (sic!)</p>
        <p><a href="https://play.google.com/store/apps/details?id=app.qrcode">Darmowe QR Skaner</a></p>
    </div>
{% endblock %}