<nav class="navbar  navbar-inverse" id="main-navbar">
    <div class="container container-fluid">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only"> Menu</span>
            <span class="icon-bar"> </span>
            <span class="icon-bar"> </span>
            <span class="icon-bar"> </span>
        </button>

        {{ image("img/logo200.png", 'class': 'navbar-brand') }}
        <a class="navbar-brand" href="{{ url(['for': 'homepage']) }}"> QR Code</a>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                {{ partial("partials/menu/profileMenu") }}
            </ul>
        </div>
    </div>
</nav>