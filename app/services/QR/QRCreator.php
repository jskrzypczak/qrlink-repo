<?php

namespace QrCode\Services\QR;

use Phalcon\Http\Request\File;
use Phalcon\Http\Request\FileInterface;
use PHPQRCode;
use QrCode\Models\User\User;

class QRCreator
{

    /**
     * @param string $text
     * @param string $fill
     * @param string $background
     * @param int $errorLevel
     * @param int $outerFrame
     * @param int $pixelPerPoint
     * @return resource
     */
    public function create($text, $fill = "#000000", $background = "#FFFFFF", $errorLevel = PHPQRCode\Constants::QR_ECLEVEL_H, $outerFrame = 2, $pixelPerPoint = 5)
    {
        $frame = PHPQRCode\QRcode::text($text, false, $errorLevel);

        $h = count($frame);
        $w = strlen($frame[0]);

        $imgW = $w + 2 * $outerFrame;
        $imgH = $h + 2 * $outerFrame;

        $base_image = imagecreatetruecolor($imgW, $imgH);

        $fillRGB = $this->hexToRGB($fill);
        $backgroundRGB = $this->hexToRGB($background);

        $background = imagecolorallocate($base_image, $backgroundRGB['red'], $backgroundRGB['green'], $backgroundRGB['blue']);
        $fill = imagecolorallocate($base_image, $fillRGB['red'], $fillRGB['green'], $fillRGB['blue']);

        imagefill($base_image, 0, 0, $background);

        for ($y = 0; $y < $h; $y++) {
            for ($x = 0; $x < $w; $x++) {
                if ($frame[$y][$x] == '1') {
                    imagesetpixel($base_image, $x + $outerFrame, $y + $outerFrame, $fill);
                }
            }
        }

        $target_image = imagecreatetruecolor($imgW * $pixelPerPoint, $imgH * $pixelPerPoint);
        imagecopyresized($target_image, $base_image, 0, 0, 0, 0, $imgW * $pixelPerPoint, $imgH * $pixelPerPoint, $imgW, $imgH
        );

        return $target_image;
    }

    private function hexToRGB($hex)
    {
        if (substr($hex, 0, 1) == "#") {
            $hex = substr($hex, 1);
        }

        $rgb['red'] = hexdec(substr($hex, 0, 2));
        $rgb['green'] = hexdec(substr($hex, 2, 2));
        $rgb['blue'] = hexdec(substr($hex, 4, 2));

        return $rgb;
    }

    public function getBase64($qr)
    {
        ob_start();
        imagepng($qr);
        $imageData = ob_get_contents();
        ob_end_clean();

        return base64_encode($imageData);
    }

    public function saveQR($qr)
    {
        $filename = date('YmdHis') . md5(rand(0, 1000) . ".png");
        $path = BASE_PATH . "/qr/" . $filename;
        imagepng($qr, $path);

        return $filename;
    }

    public function addImageToQR($qr, File $file)
    {
        $name = User::getCurrentUserId() . "_" . date("YmdHis") . "_" . md5(rand(0, 1000)) . "." . $file->getExtension();
        $path = BASE_PATH . "/images/" . $name;

        $file->moveTo($path);
        $image = $this->imageCreateFromFile($path);
        imagesavealpha($image, true);


        $size = getimagesize($path);
        $imageSize['width'] = $size[0];
        $imageSize['height'] = $size[1];

        $qrSize['width'] = $qrSize['height'] = imagesx($qr);

        $ratio = sqrt((0.11 * $qrSize['width'] * $qrSize['height']) / ($imageSize['width'] * $imageSize['height']));

        $image = $this->resize($image, intval($imageSize['width'] * $ratio), intval($imageSize['height'] * $ratio));
        imagesavealpha($image, true);

        $qr = $this->copyImageIntoQR($qr, $image);
        imagedestroy($image);

        return $qr;
    }

    private function imageCreateFromFile($filename)
    {
        switch (strtolower(pathinfo($filename, PATHINFO_EXTENSION))) {
            case 'jpeg':
            case 'jpg':
                return imagecreatefromjpeg($filename);
                break;

            case 'png':
                return imagecreatefrompng($filename);
                break;
        }

        return false;
    }

    private function resize($im, $dst_width, $dst_height) {
        $width = imagesx($im);
        $height = imagesy($im);

        $newImg = imagecreatetruecolor($dst_width, $dst_height);

        imagealphablending($newImg, false);
        imagesavealpha($newImg, true);
        $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
        imagefilledrectangle($newImg, 0, 0, $width, $height, $transparent);
        imagecopyresampled($newImg, $im, 0, 0, 0, 0, $dst_width, $dst_height, $width, $height);

        return $newImg;
    }

    private function copyImageIntoQR($qr, $image)
    {
        $qrSize['width'] = imagesx($qr);
        $qrSize['height'] = imagesy($qr);
        $imageSize['width'] = imagesx($image);
        $imageSize['height'] = imagesy($image);

        $this->imagecopymerge_alpha(
            $qr,$image,
            ($qrSize['width'] - $imageSize['width']) / 2, ($qrSize['height'] - $imageSize['height']) / 2,
            0,0,
            $imageSize['width'], $imageSize['height'],
            100
        );

        return $qr;
    }

    private function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
        $cut = imagecreatetruecolor($src_w, $src_h);
        imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);
        imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);
        imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
    }
}